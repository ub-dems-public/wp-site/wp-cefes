<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Flash
 */

get_header(); ?>

	<?php
	/**
	 * flash_before_body_content hook
	 */
	do_action( 'flash_before_body_content' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		
			<iframe src="https://script.google.com/a/macros/unimib.it/s/AKfycbzwrmPqYxXKYE2IdJ3LU5gzZjEM8_XpZVGW3I_xwT1-MG2ha6k/exec" frameborder="0" scrolling="auto" width="100%" height="2150">
			Your browser doesn't support iFrames: you can reach the application form by clicking <a href="https://script.google.com/a/macros/unimib.it/s/AKfycbzwrmPqYxXKYE2IdJ3LU5gzZjEM8_XpZVGW3I_xwT1-MG2ha6k/exec" target="_blank">here</a></iframe>


	

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	/**
	 * flash_after_body_content hook
	 */
	do_action( 'flash_after_body_content' ); ?>

<?php
get_sidebar();
get_sidebar( 'left' );
get_footer();
